import {useState, useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom'
import UserContext from '../UserContext';



export default function AppNavbar() {


	const {user, isAdmin} = useContext(UserContext);



	return(

		<Navbar bg="success" expand="lg" variant="dark" className="mb-5">
			<Navbar.Brand href="/" className="mx-3">
			        <img
			          alt=""
			          src="/logo.png"
			          width="30"
			          height="30"
			          className="d-inline-block align-top"
			        />{' '}
			      TechShop
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto">
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					<Nav.Link as={Link} to="/products">Product</Nav.Link>



					{(user.accessToken !== null && user.isAdmin === 'false') ?
					<>
						<Nav.Link as={Link} to="/cart">My Cart</Nav.Link>
						<Nav.Link as={Link} to="/orderHistory">Order History</Nav.Link>
					</>
						:
						null
					}
					


					{(user.accessToken === null) ?

					<>
						<Nav.Link as={Link} to="/login">Login</Nav.Link>
						<Nav.Link as={Link} to="/register">Register</Nav.Link>
					</>	
						:
						<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
					}

				</Nav>
			</Navbar.Collapse>
			
		</Navbar>

		)
};


