import { useState, useEffect } from 'react';
import ProductCard from './ProductCard';


export default function UserView({productData}) {

	const [ products, setProducts ] = useState([])

	 
	useEffect(() => {

		const productsArr = productData.map(product => {
			
			if(product.isActive === true) {
				return(

					<ProductCard key={product._id} productProp={product}/>
					)
			}else {
				return null;
			}
		})


		setProducts(productsArr)


	}, [productData])


	return(
		<>
			{products}
		</>
		)
}