import {useState, useEffect} from 'react';
import { Card, Button, Row, Col, Container} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';


export default function ProductCard({productProp}) {

	const {_id, name, description, price} = productProp

	return(
		<Container>	
			<Row className="justify-content-md-center">
				<Col xs lg="6">
					<Card className="ProductCard my-4 text-white bg-dark">
						<Card.Body>
							<Card.Title> {name} </Card.Title>

							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}.</Card.Text>

							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							<Button variant="info" as={Link} to={`/products/${_id}`} >Details</Button>

							
							
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

		)
}



ProductCard.propTypes = {

	productProp: PropTypes.shape({
		
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}