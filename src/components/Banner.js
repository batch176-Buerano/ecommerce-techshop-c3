//React bootstraps composnets
import { Button, Row, Col, Container, Carousel } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(props) {
	return (
		<Container fluid>	
				<Row>
					<Col className="p-5 mt-3" md={3}>

						<h1 className="pt-5">Welcome to</h1>
						<h1 className="mb-3">TechShop</h1>
						
						<p className="mb-3">Buy your gadgets and accessories</p>
						<Button variant="info" as={ Link } to="/login">New Products</Button>
					</Col>
					<Col className="p-3 mt-2" md={9}>
						<Carousel className="Carousel">
						  <Carousel.Item interval={1000}>
						    <img
						      className="w-100 Carousel1"
						      src="https://m.media-amazon.com/images/I/814GxhunmDL._AC_SL1500_.jpg"
						      alt="First slide"
						    />

						  </Carousel.Item>
						  <Carousel.Item interval={500}>
						    <img
						      className="d-block w-100 Carousel1"
						      src="https://m.media-amazon.com/images/I/71tyxol6XZL._AC_SL1500_.jpg"
						      alt="Second slide"
						    />

						  </Carousel.Item>
						  <Carousel.Item>
						    <img
						      className="d-block w-100 Carousel1"
						      src="https://m.media-amazon.com/images/I/81wm88ThUsL._AC_SL1500_.jpg"
						      alt="Third slide"
						    />

						  </Carousel.Item>
						</Carousel>
					</Col>
				</Row>
			</Container>


		)
}