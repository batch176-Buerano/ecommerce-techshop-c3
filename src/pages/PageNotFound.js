import { Row, Col} from 'react-bootstrap';
import {NavLink} from 'react-router-dom'

export default function PageNotFound() {
    return(

        <Row>
            <Col>
                <h1> 404 - Page not found</h1>
                <p>The page you are looking for connot found</p>
                <p>Go back to <NavLink to="/">homepage</NavLink></p>
            </Col>
        </Row>


        )
}
