import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { flatten, isEmpty } from 'lodash';
import {useNavigate} from 'react-router-dom'



export default function CartView(props) {

	const navigate = useNavigate();

	const [cartItems, setCartItems] = useState([])
	const [loading, setLoading] = useState(false)

	const fetchData = () => {
		setLoading(true)

		fetch(`https://ecommers-jjbro.herokuapp.com/users/myCart/${localStorage.getItem('userId')}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
		},

		})
		.then(res => res.json())
		.then(data => {


			setCartItems(data)
			setLoading(false)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])

	if(loading) return null

	const handleDecrement = (id) => {
		if(cartItems === null) return
			const newCartItems = cartItems.map(item => {
				if(item._id === id){
					let newTotalAmount = 0
					return {
						...item,
						products: [{
							...item.products[0],
							quantity: (() => {
								if(item.products[0].quantity !== 1){
									const newQuantity = item.products[0].quantity-1
									newTotalAmount = item.totalAmount - item.products[0].productId.price
									return newQuantity
								}
								newTotalAmount = item.totalAmount
								return item.products[0].quantity
							})()
						}],
						totalAmount: newTotalAmount
					}
				}
				return item
			})
		setCartItems(newCartItems)
	};

	const handleIncrement = (id) => {
		if(cartItems === null) return
			const newCartItems = cartItems.map(item => {
				if(item._id === id){
					let newTotalAmount = 0
					return {
						//...item - is a spread operator, copy all properties item
						...item,
						products: [{
							//...item - is a spread operator, copy all properties products.
							...item.products[0],
							quantity: (() => {
								if(item.products[0].quantity !== 10){
									const newQuantity = item.products[0].quantity+1
									newTotalAmount = item.totalAmount + item.products[0].productId.price
									return newQuantity
								}
								newTotalAmount = item.totalAmount
								return item.products[0].quantity
							})()
						}],
						//() => {}
						totalAmount: newTotalAmount
					}
				}
				return item
			})
		setCartItems(newCartItems)
	};


const deleteProduct = (id) => {
	fetch(`https://ecommers-jjbro.herokuapp.com/users/deleteCart/${id}`,{
		method: 'DELETE',
		headers: {
			Authorization: `Bearer ${ localStorage.getItem('accessToken')}`
		}
	})
	.then(data =>{
		if(data.ok) {
			Swal.fire({
				title: 'success',
				icon: 'success',
				text: 'product successfully removed'
			})
			fetchData()
		}else {
			Swal.fire({
				title: 'error',
				icon: 'error',
				text: 'Something went wrong'
			})
			fetchData()
		}
	})
}

	
	const handleCheckout = () => {
		const products = flatten(cartItems.map(item => {
			return item.products
		}))
		const body = {
			userId: localStorage.getItem('userId'),
			products: products
		}

		fetch(`https://ecommers-jjbro.herokuapp.com/users/orders`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
		},
		body: JSON.stringify(body)

		})
		.then(res => res.json())
		.then(data => {
			navigate('/orderHistory')
			fetchData()
		})
	}



	return(
		<>
			<div className="text-center my-4">
				<h1>My Cart</h1>
				
			</div>
			
			<Table striped bordered hover responsive>
				<thead className="bg-success text-white text-center my-4">
					<tr>	
						<th>NAME</th>
						<th>DESCRIPTION</th>
						<th>PRICE</th>
						<th>Order Quantity</th>
						<th>Sub Total</th>
						<th colSpan="2" className="text-center">Delete</th>
					</tr>
				</thead>

				<tbody className="text-white">

					{cartItems.map((item) => {
						 
						return <tr key={item._id} className="text-center">
							<td>{item.products?.[0]?.productId.name}</td>
							<td>{item.products?.[0]?.productId.description}</td>
							<td>{item.products?.[0]?.productId.price}</td>
							<td>
								<span className="d-flex p-2 justify-content-between align-items-center">
									<Button type="button" onClick={() => handleDecrement(item._id)} className="input-group-text">-</Button>
									<span  className="from-control text-center mx-5">{item.products?.[0]?.quantity}</span>
									<Button type="button" onClick={() => handleIncrement(item._id)} className="input-group-text">+</Button>
								</span>
							</td>
							<td>{item.totalAmount}</td>
							<td>
								<Button type="button" onClick={() => deleteProduct(item._id)} className="input-group-text bg-danger">Remove</Button>
							</td>
						</tr>
					}) }
				</tbody>
			</Table>

				{!isEmpty(cartItems) && <Button type="button" onClick={handleCheckout} className="input-group-text bg-info">Check Out</Button>}


		</>

		)
}