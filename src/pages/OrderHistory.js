import { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { format } from 'date-fns';


export default function OrderHistoryView(props) {

	const [orderHistoryItems, setOrderHistoryItems] = useState([])
	const [loading, setLoading] = useState(false)

	const fetchData = () => {
		setLoading(true)

		fetch(`https://ecommers-jjbro.herokuapp.com/users/myOrder/${localStorage.getItem('userId')}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
		},

		})
		.then(res => res.json())
		.then(data => {

			setOrderHistoryItems(data)
			setLoading(false)
		})
	}
	
	useEffect(() => {
		fetchData()
	}, [])

	if(loading) return null


	return(
		<>
			<div className="text-center my-4">
				<h1>Order History</h1>
				
			</div>
			
			
				{orderHistoryItems.map((item, index) => {
					return (
						<Container>	
							<Row className="justify-content-md-center">
								<Col xs lg="6">
									<div className="card mb-3 text-white bg-dark" key={item._id}>
									  <div className="card-header">
									    {`Order #${index+1} - Purchased On - ${format(new Date(item.purchasedOn), 'MMM dd, yyyy' )}`}
									  </div>
									  <div className="card-body">
									    <h5 className="card-title">Items:</h5>
									    {item.products.map((product) => {
									    	return <p className="card-text">
									    		{`${product.productId.name} - Quantity: ${product.quantity}`}
									    	</p>
									    })}

									    <h5 className="card-text">{`Total: PHP ${item.totalAmount}`}</h5>
									  </div>
									</div>
								</Col>
							</Row>
						</Container>
						)

				})}
		</>

		)
}