import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'
import {Navigate, useNavigate} from 'react-router-dom';


export default function Register() {

	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ mobileNo, setMobileNo ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');


	const [ isActive, setIsActive ] = useState(true);

	useEffect(() => {

		if((email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [email, password, verifyPassword])


	const registerUser = (e) => {
			e.preventDefault();

			fetch('https://ecommers-jjbro.herokuapp.com/users/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password,
					mobileNo: mobileNo
				})
			})
			.then(res => res.json())
			.then(data => {
				

				if(data){
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Register is successfull'
					})


					navigate('/login')
					//window.location.reload() --> alternative to fetchData
					
				}else {
					Swal.fire({
						title: 'error',
						icon: 'error',
						text: 'Please try again'
					})

				}

				setFirstName('')
				setLastName('')
				setEmail('')
				setPassword('')
				setMobileNo('')
			})


		}


	return(

		(user.accessToken !== null) ?

		<Navigate to="/products" />

		:

		<Form onSubmit={e => registerUser(e)} className="">
		    <h1>Register</h1>

		    <Form.Group className="size1">
		    	<Form.Label>First Name</Form.Label>
		    	<Form.Control 
		    	    type="text"
		    	    placeholder="Enter your First Name"
		    	    required
		    	    value={firstName}
		    	    onChange={e => setFirstName(e.target.value)}
		    	    />
		    </Form.Group>

		    <Form.Group className="size1">
		    	<Form.Label>Last Name</Form.Label>
		    	<Form.Control 
		    	    type="text"
		    	    placeholder="Enter your Last Name"
		    	    required
		    	    value={lastName}
		    	    onChange={e => setLastName(e.target.value)}
		    	    />
		    </Form.Group>

		    <Form.Group className="size1">
		    	<Form.Label>Mobile Number</Form.Label>
		    	<Form.Control 
		    	    type="Number"
		    	    placeholder="Enter your Mobile Number"
		    	    required
		    	    value={mobileNo}
		    	    onChange={e => setMobileNo(e.target.value)}
		    	    />
		    </Form.Group>

			<Form.Group className="size1">
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
				    type="email"
				    placeholder="Enter email"
				    required
				    value={email}
				    onChange={e => setEmail(e.target.value)}
				    />
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group className="size1">
				<Form.Label>Password</Form.Label>
				<Form.Control 
				    type="password"
				    placeholder="Enter your Password"
				    required
				    value={password}
				    onChange={e => setPassword(e.target.value)}
				    />
			</Form.Group>

			<Form.Group className="size1">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
				    type="password"
				    placeholder="Verify Password"
				    required
				    value={verifyPassword}
				    onChange={e => setVerifyPassword(e.target.value)}
				    />
			</Form.Group>

			{isActive ?
				<Button variant="info" type="submit" className="mt-3">Submit</Button>
				:
				<Button variant="info" type="submit" className="mt-3" disabled>Submit</Button>
			}
			
			
		</Form>



		)
}