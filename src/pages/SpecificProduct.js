import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useParams, Link, useNavigate } from 'react-router-dom';


export default function SpecificProduct() {

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const [quantity, setQuantity] = useState(1);


	useEffect(() => {

		fetch(`https://ecommers-jjbro.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, []);

	const handleDecrement = () => {
		if(quantity > 1){
			setQuantity(prevCount => prevCount - 1 );
		}
	};

	const handleIncrement = () => {
		if(quantity < 10) {	
			setQuantity(prevCount => prevCount + 1 );
		}
	};

	const { user } = useContext(UserContext);

	const addToCart = (productId) => {

			fetch('https://ecommers-jjbro.herokuapp.com/users/cart', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
				},
				body: JSON.stringify({
					products: [
	          {
	            productId,
	            quantity: quantity
	          }
	        ]
				})
			})
			.then(res => res.json())
			.then(data => {
				

				if(data){
					Swal.fire({
						title: 'Successfully added to cart!',
						icon: 'success',
						text: `You have successfully added to cart!`
					})

					navigate('/cart')
				} else {
					Swal.fire({
						title: 'error!',
						icon: 'error',
						text: 'Something went wrong, please try again'
					})
				}
			})
		}


	return(
		<Container>
			<Row className="justify-content-md-center">
				<Col xs lg="6">
					<Card className="ProductCard my-4 text-white bg-dark">
						<Card.Header>
							<h4>{ name }</h4>
						</Card.Header>

						<Card.Body>
							<Card.Text>{ description }</Card.Text>
							<h6>Price: Php { price } </h6>
							<h6>Available: 10</h6>
						</Card.Body>

						<Card.Header>
							<Button type="button" onClick={handleDecrement} className="input-group-text">-</Button>
							<span  className="from-control text-center mx-5">{quantity}</span>
							<Button type="button" onClick={handleIncrement} className="input-group-text">+</Button>
						</Card.Header>


						<Card.Footer>
						{ user.accessToken !== null ?
							<Button variant="info" onClick={() => addToCart(productId)}>Add to Cart</Button>
							:
							<Button variant="warning" as={ Link } to="/login">Login to Buy</Button>
						 }
						 		
						</Card.Footer>
					</Card>
				</Col>
			</Row>
		</Container>

		)
}