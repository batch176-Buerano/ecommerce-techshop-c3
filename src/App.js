import {useState} from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import ProductPage from './pages/ProductPage';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import SpecificProduct from './pages/SpecificProduct';
import Cart from './pages/Cart';
import OrderHistory from './pages/OrderHistory';
import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext';



import { BrowserRouter, Routes, Route } from 'react-router-dom';


function App() {

    const [user, setUser] = useState({
        accessToken: localStorage.getItem('accessToken'),
        isAdmin: localStorage.getItem('isAdmin') //=== 'true'
    })


    const unsetUser = () => {
        localStorage.clear();
    }


  return (
    <UserProvider value = {{user, setUser, unsetUser}} >
        <BrowserRouter>
            <AppNavbar />
            <Container>
                <Routes>
                    <Route path="/" element={ <Home /> }/>
                    <Route path="/products" element={ <ProductPage /> }/>
                    <Route path="/register" element={ <Register /> }/>
                    <Route path="/login" element={ <Login /> }/>
                    <Route path="/logout" element={ <Logout /> }/>
                    <Route path="/products/:productId" element={ <SpecificProduct /> }/>
                    <Route path="*" element={ <PageNotFound />} />
                    <Route path="/cart" element={ <Cart /> }/>
                    <Route path="/orderHistory" element={ <OrderHistory /> }/>
                </Routes>
            </Container>
        </BrowserRouter>
    </UserProvider>
  );
}

export default App;
